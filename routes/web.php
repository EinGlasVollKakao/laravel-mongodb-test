<?php

use App\Http\Controllers\FlightController;
use App\Http\Controllers\LoftController;
use App\Http\Controllers\MongoFlightDataController;
use App\Http\Controllers\PigeonController;
use App\Http\Controllers\SeasonController;
use App\Http\Controllers\UserController;
use App\Http\Middleware\VerifyCsrfToken;
use App\Models\Flight;
use App\Models\MongoFlightData;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/lofts');
});

Route::get('/lofts', [LoftController::class, 'index']);
Route::get('/loft/{id}', [LoftController::class, 'show']);

Route::get('/season/{id}', [SeasonController::class, 'show']);

Route::get('/pigeons', [PigeonController::class, 'index']);

Route::get('/flight/{id}', [FlightController::class, 'show']);
Route::get('/flight/{id}/filter', [FlightController::class, 'filter']);

Route::get('/flight/{id}/mongo', [MongoFlightDataController::class, 'show']);

Route::get('/users', [UserController::class, 'index']);

Route::get('/test', function () {
    \App\Models\MongoFlight::saveFlight(4);
});


