<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_data', function (Blueprint $table) {
            $table->foreignId('flight_id')->constrained();
            $table->foreignId('pigeon_id')->constrained();
            $table->timestamp('basketed_at')->nullable();
            $table->timestamp('arrived_at')->nullable();
            $table->timestamps();

            $table->primary(['flight_id', 'pigeon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_data');
    }
}
