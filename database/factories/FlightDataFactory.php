<?php

namespace Database\Factories;

use App\Models\FlightData;
use App\Models\Pigeon;
use Illuminate\Database\Eloquent\Factories\Factory;

class FlightDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FlightData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $basketedAt = $this->faker->dateTime();
        return [
            'pigeon_id' => $this->faker->numberBetween(1, Pigeon::count()),
            'basketed_at' => $basketedAt,
            'arrived_at' => $this->faker->randomElement([null, $this->faker->dateTimeBetween($basketedAt)]),
        ];
    }
}
