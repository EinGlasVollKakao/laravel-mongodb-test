<?php

namespace Database\Seeders;

use App\Models\Flight;
use App\Models\FlightData;
use App\Models\Loft;
use App\Models\Pigeon;
use App\Models\Season;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        ini_set('memory_limit', '-1');
        \DB::disableQueryLog();
        \DB::beginTransaction();

        User::factory()
            ->count(125000)
            ->create();

        \DB::commit();
    }
}
// 9:55 10
