<?php

namespace Database\Seeders;

use App\Models\Flight;
use App\Models\FlightData;
use App\Models\Loft;
use App\Models\Pigeon;
use App\Models\Season;
use App\Models\User;
use Illuminate\Database\Seeder;
use Faker\Factory;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        ini_set('memory_limit', '-1');
        \DB::disableQueryLog();
        \DB::beginTransaction();
        $faker = Factory::create();

        $basketedAt = $faker->dateTime();
        Loft::factory()
            ->count(10)
            ->has(
                Season::factory()
                    ->count(5)
                    ->has(
                        Flight::factory()
                            ->count(5)
                            ->hasAttached(
                                Pigeon::factory()->count(5000),
                                [
                                    'basketed_at' => $basketedAt,
                                    'arrived_at' => $faker->randomElement([null, $faker->dateTimeBetween($basketedAt)])
                                ]
                            )
                    )
            )->create();

        \DB::commit();
    }
}
// 9:55 10
