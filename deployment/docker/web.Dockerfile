FROM beth420/nginx-php:1.0.1
ARG NPM_DEPLOYMENT=prod
ARG APP_ENV=local
ARG BUILD_TYPE=local

# nginx dev configs
USER root
RUN if [ "${APP_ENV}" = "development" ] || [ "${APP_ENV}" = "staging" ]; then \
    sed -i '/forward everything/i # automated authentication integration\n \
    location ~ ^/(login|register) {\n \
        satisfy any;\n \
        allow 194.208.49.94;\n \
        deny all;\n \
        auth_basic "Page Protected";\n \
        auth_basic_user_file "/etc/nginx/.htpasswd";\n \
        try_files $uri $uri/ /index.php?$query_string;\n \
    }\n' /etc/nginx/sites-available/default; \
    sed -i 's/\r$//' /etc/nginx/sites-available/default; \
    cat /etc/nginx/sites-available/default; \
    fi

# install debugger only in local environment
RUN if [ "${APP_ENV}" = "local" ]; then \
    pecl install xdebug; \
    docker-php-ext-enable xdebug;  \
    fi

RUN apt-get update && \
    apt-get install -y libcurl4-openssl-dev libssl-dev

# install mongodb php extension
RUN pecl install mongodb && \
    docker-php-ext-enable mongodb


# copy all files into the container
USER web
COPY --chown=web:web . .

# aws fargate 1.4.0 needs a definition of all volumes per container
# to properly find them and to set permissions.
VOLUME ["/var/log/nginx"]
VOLUME ["/laravel/storage/logs"]
VOLUME ["/logs"]

# run installation scripts inside container if
# we require a fully built container
RUN if [ "${BUILD_TYPE}" = "pipeline" ]; then \
    composer install; \
    npm install; \
    npm run "$NPM_DEPLOYMENT"; \
    chmod 777 -R ./vendor && chown web:web -R ./vendor; \
    chmod 777 -R ./node_modules && chown web:web -R ./node_modules; \
    ls -la ./resources/views; \
    fi

# show content for debugging
RUN ls -la

# I give up. it just won't work without root.
# nginx just cannot write its logs
#USER root
