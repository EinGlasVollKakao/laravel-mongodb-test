<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loft extends Model
{
    use HasFactory;

    public function seasons()
    {
        return $this->hasMany(Season::class);
    }

}
