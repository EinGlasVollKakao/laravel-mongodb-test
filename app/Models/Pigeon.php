<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pigeon extends Model
{
    use HasFactory;

    public function flights()
    {
        return $this->belongsToMany(Flight::class, 'flight_data')->withPivot('basketed_at', 'arrived_at');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
