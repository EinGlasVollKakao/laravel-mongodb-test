<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class MongoFlight extends Model
{
    protected $connection = 'mongodb';

    protected $guarded = [];

    public function pigeons()
    {
        return $this->embedsMany(MongoPigeon::class);
    }


    public static function saveFlight($flightId)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3000');

        $flight = Flight::findOrFail($flightId);
        $pigeons = $flight->pigeons()->with('user')->get();


        $mongoFlight = new MongoFlight();
        $mongoFlight->_id = $flight->id;
        $mongoFlight->name = $flight->name;
        $mongoFlight->type = $flight->type;


        $mongoPigeons = [];

        foreach ($pigeons as $pigeon) {
            $mongoPigeon = new MongoPigeon();

            $mongoPigeon->_id = $pigeon->id;
            $mongoPigeon->name = $pigeon->name;
            $mongoPigeon->ring_number = $pigeon->ring_number;
            $mongoPigeon->owner_name = $pigeon->user->name;
            $mongoPigeon->basketed_at = $pigeon->pivot->basketed_at;
            $mongoPigeon->arrived_at = $pigeon->pivot->arrived_at;


            array_push($mongoPigeons, $mongoPigeon->toArray());
        }



        $mongoFlight->pigeons = $mongoPigeons;
        $mongoFlight->save();


        return 'oke';
    }
}
