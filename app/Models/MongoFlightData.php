<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class MongoFlightData extends Model
{
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';

    protected $guarded = [];

    public function pigeons()
    {
        return $this->embedsMany(MongoPigeon::class);
    }

    public static function saveFlight($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3000');
        $flight = Flight::find($id);

        $pigeons = $flight->pigeons()->with('user')->get();

        $insertData = [];

        foreach ($pigeons as $pigeon) {
            $data = [
                '_id' => "f" . $flight->id . "p" . $pigeon->id,

                'flight_id' => $flight->id,
                'flight_name' => $flight->name,
                'flight_type' => $flight->type,

                'pigeon_id' => $pigeon->id,
                'pigeon_name' => $pigeon->name,
                'pigeon_ring_number' => $pigeon->ring_number,

                'user_id' => $pigeon->user_id,
                'user_name' => $pigeon->user->name,

                'basketed_at' => $pigeon->pivot->basketed_at,
                'arrived_at' => $pigeon->pivot->arrived_at,
            ];
            array_push($insertData, $data);
        }

        MongoFlightData::insert($insertData);
    }


    public static function saveFlightNested($id)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '3000');
        $flight = Flight::find($id);

        $pigeons = $flight->pigeons()->with('user')->get();

        $insertData = [
            '_id' => $flight->id,
            'name' => $flight->name,
            'type'=> $flight->type,
            'pigeons' => [],
        ];

        foreach ($pigeons as $pigeon) {
            $data = [
                '_id' => $pigeon->id,
                'pigeon_name' => $pigeon->name,
                'pigeon_ring_number' => $pigeon->ring_number,

                'user_id' => $pigeon->user_id,
                'user_name' => $pigeon->user->name,

                'basketed_at' => $pigeon->pivot->basketed_at,
                'arrived_at' => $pigeon->pivot->arrived_at,
            ];
            array_push($insertData["pigeons"], $data);
        }

        MongoFlightData::insert($insertData);
    }
}
