<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class MongoPigeon extends Model
{
    protected $primaryKey = '_id';
    protected $connection = 'mongodb';

    protected $guarded = [];
}
