<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    use HasFactory;

    public function season()
    {
        return $this->belongsTo(Season::class);
    }

    public function pigeons()
    {
        return $this->belongsToMany(Pigeon::class, 'flight_data')->withPivot('basketed_at', 'arrived_at');
    }

}
