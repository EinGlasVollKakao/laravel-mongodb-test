<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    use HasFactory;

    public function loft()
    {
        return $this->belongsTo(Loft::class);
    }

    public function flights()
    {
        return $this->hasMany(Flight::class);
    }
}
