<?php

namespace App\Http\Controllers;

use App\Models\Pigeon;
use Illuminate\Http\Request;

class PigeonController extends Controller
{
    public function index()
    {
        return view('pigeon.index', [
            'pigeons' => Pigeon::paginate(19),
        ]);
    }
}
