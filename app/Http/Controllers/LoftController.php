<?php

namespace App\Http\Controllers;

use App\Models\Loft;
use Illuminate\Http\Request;

class LoftController extends Controller
{

    public function index()
    {
        return view('loft.index', [
            'lofts' => Loft::all(),
        ]);
    }

    public function show($id)
    {
        return view('loft.show', [
            'loft' => Loft::find($id),
        ]);
    }
}
