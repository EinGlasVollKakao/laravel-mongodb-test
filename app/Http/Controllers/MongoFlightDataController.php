<?php

namespace App\Http\Controllers;

use App\Models\Flight;
use App\Models\MongoFlight;
use App\Models\MongoFlightData;
use App\Models\MongoPigeon;
use App\Models\Pigeon;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class MongoFlightDataController extends Controller
{
    public function show(int $id, Request $request)
    {
//        $perPage = 30;
//        $currentPage = $request->input('page', 1);
//        $offset = ($currentPage - 1) * $perPage;


        $flight = MongoFlight::where('_id', $id)->first();

        $query = $request->get('query');
        $queryRegexp = "/.*${query}.*/i";

        if ($query == null) {
            MongoPigeon::where(''->pigeons->paginate(30);
        } else {
            $queryResult = $flight
                ->pigeons()
                ->where('name',$query);

            $total = $queryResult->count();
            $result = $queryResult
                ->skip($offset)
                ->take($perPage);

//            $flight = MongoFlightData::where('_id', $id)
//                ->where(function ($query) use ($queryRegexp) {
//                    return $query
//                        ->where('pigeon_name', 'regexp', $queryRegexp)
//                        ->orWhere('user_name', 'regexp', $queryRegexp);
////                        ->raw()->aggregate([
////                            ['$addFields' => ['pigeon_ring_number_str' => ['$toString' => '$pigeon_ring_number']]],
////                            ['$match' => ['pigeon_ring_number_str' => $queryRegexp]]
////                        ]);
//                })
//                ->orderBy('pigeon_id')
//                ->paginate(30);
        }

//        $pigeons = new LengthAwarePaginator($result, $total, $perPage, $currentPage, [
//            'path' => Paginator::resolveCurrentPath(),
//            'pageName' => 'page',
//        ]);


        if ($request->ajax()) {
            return view('flight.mongo.results',
                compact('flight', 'pigeons'));
        }

        return view('flight.mongo.show',
            compact('flight', 'pigeons'));

    }
}
