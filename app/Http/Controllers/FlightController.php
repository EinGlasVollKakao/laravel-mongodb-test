<?php

namespace App\Http\Controllers;

use App\Models\Flight;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    public function show($id, Request $request)
    {
        $flight = Flight::findOrFail($id);

        $query = $request->get('query');
        if ($query == null) {
            $pigeons = $flight->pigeons()->with('user')->paginate(30);
        } else {
            $filter = "%${query}%";
            $pigeons = $flight->pigeons()->with('user')
                ->where(function ($query) use ($filter) {
                    return $query
                        ->where('name', 'like', $filter)
                        ->orWhere('ring_number', 'like', $filter)
                        ->orWhereRelation('user', 'name', 'like', $filter);
                })
                ->paginate(30);
        }


        if ($request->ajax()) {
            return view('flight.results',
                compact('flight', 'pigeons'));
        }

        return view('flight.show',
            compact('flight', 'pigeons'));
    }
}
