<div class="table-container">
    <table>
        <tr>
            <th>Pigeon Name</th>
            <th>Ring Number</th>
            <th>Pigeon Owner</th>
            <th>Basketed at</th>
            <th>Arrived at</th>
        </tr>
        @foreach($pigeons as $pigeon)
            <tr>
                <td>{{$pigeon->name}}</td>
                <td>{{$pigeon->ring_number}}</td>
                <td>{{$pigeon->owner_name}}</td>
                <td>{{$pigeon->basketed_at}}</td>
                <td>{{$pigeon->arrived_at}}</td>
            </tr>
        @endforeach
    </table>
</div>
{{ $pigeons->withQueryString()->links() }}
