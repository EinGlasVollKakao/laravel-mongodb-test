@extends('layouts.app')

@section('title', $flight->name . ' -->MYSQL')


@section('content')

    <div class="search-container">
        <div class="header">
            <h1 id="search-header">{{$flight->name . ':'}}</h1>
        </div>

        <div class="search">
            <form id="search" action="{{URL::to("/flight/{$flight->id}")}}" method="GET">
                <input
                    type="text"
                    name="query"
                    value="{{$query ?? ''}}"
                    autofocus
                    onfocus="this.select()"
                >
                <a id="clear-filter" href="{{URL::to("/flight/{$flight->id}")}}">clear</a>
            </form>
        </div>
    </div>

    <div id="ajax-results">
        @include('flight.results')
    </div>

    <p>mysql</p>

    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.paginate-link',  function(event) {
                event.preventDefault();

                let url = $(this).attr('href');

                fetchData(url);
            });
        });

        $('#search').submit(function (event) {
            event.preventDefault();

            let form = $(this);
            let url = form.attr('action');
            let data = form.serialize();

            fetchData(url, data);
        });

        function changeUrl(url) {
            let obj = {
                url: url
            }
            history.pushState(obj, '', obj.url);
        }

        function fetchData(url, data = "") {
            if (data !== "")
            {
                if (url.includes('?')) {
                    url = url + '&' + data;
                } else {
                    url = url + '?' + data;
                }
            }

            $.ajax({
                url: url,
                success: function (data) {
                    $('#ajax-results').html(data);
                }
            });
        }
    </script>
@endsection
