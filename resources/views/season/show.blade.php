@extends('layouts.app')

@section('title', 'Season: ' . $season->name)

@section('header', $season->name)

@section('content')
    <a href="/loft/{{$season->loft->id}}"><h3>Loft: {{$season->loft->name}}</h3></a>
    <br>
    <h2>Flights:</h2>
    <ul>
        @foreach($season->flights as $flight)
            <li><a href="/flight/{{$flight->id}}">{{$flight->name}}</a> <a href="/flight/{{$flight->id}}/mongo">(Mongo)</a></li>
        @endforeach
    </ul>
@endsection
