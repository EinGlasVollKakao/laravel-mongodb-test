<html lang="en">
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" type="text/css" href="{{url('/css/style.css')}}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
</head>
<body>
<div class="links">
    <ul>
        <li><a
                title="Back"
                href="{{url()->previous()}}">
                ←
            </a></li>

        <li><a
                href="/lofts"
                class="{{Request::is('lofts') ? 'disabled-link' : ''}}">
                All lofts
            </a></li>

        <li><a
                href="/pigeons"
                class="{{Request::is('pigeons') ? 'disabled-link' : ''}}">
                All pigeons
            </a></li>

        <li><a
                href="/users"
                class="{{Request::is('users') ? 'disabled-link' : ''}}">
                All users
            </a></li>
    </ul>
</div>
<hr>
<div class="container">
    @hasSection('header')
    <h1>@yield('header')</h1>
    @endif

    @yield('content')
</div>
</body>
</html>
