@if ($paginator->hasPages())
    <div class="links paginator">
        <ul>
            <li><a
                    href="{{ $paginator->previousPageUrl() }}"
                    class="{{ $paginator->onFirstPage() ? 'disabled-link' : '' }}"
                    rel="prev">
                    ← Previous
                </a></li>

            <li><a
                    href="{{ $paginator->nextPageUrl() }}"
                    class="{{ $paginator->hasMorePages() ? '' : 'disabled-link' }}"
                    rel="next">
                    Next →
                </a></li>
        </ul>
    </div>
@endif
