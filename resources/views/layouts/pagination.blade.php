@if ($paginator->hasPages())
    <div class="links paginator">
        <ul>
            <li><a
                    href="{{ $paginator->previousPageUrl() }}"
                    class="{{ $paginator->onFirstPage() ? 'disabled-link' : 'paginate-link' }}"
                    rel="prev">
                    ← Previous
                </a></li>

            <li><p>
                    Showing
                    {{ $paginator->firstItem() }}
                    to
                    {{ $paginator->lastItem() }}
                    of
                    {{ $paginator->total() }}
                </p></li>

            <li><a
                    href="{{ $paginator->nextPageUrl() }}"
                    class="{{ $paginator->hasMorePages() ? 'paginate-link' : 'disabled-link' }}"
                    rel="next">
                    Next →
                </a></li>
        </ul>
    </div>
@endif
