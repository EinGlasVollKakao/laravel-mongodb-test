@extends('layouts.app')

@section('title', $loft->name)

@section('header', $loft->name)

@section('content')
    <h3>Seasons:</h3>

    <ul>
        @foreach($loft->seasons as $season)
            <li><a href="/season/{{$season->id}}">{{$season->name}}</a></li>
        @endforeach
    </ul>
@endsection
