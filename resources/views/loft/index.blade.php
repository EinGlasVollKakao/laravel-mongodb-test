@extends('layouts.app')
@section('title', 'All lofts')

@section('header', 'All lofts:')

@section('content')
<ul>
    @foreach($lofts as $loft)
        <li><a href="/loft/{{$loft->id}}">{{$loft->name}}</a></li>
    @endforeach
</ul>
@endsection
