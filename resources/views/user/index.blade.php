@extends('layouts.app')

@section('title', 'All users')

@section('header', 'All users:')

@section('content')
    <ul>
        @foreach($users as $user)
            <li>{{$user->name}}</li>
        @endforeach
    </ul>

    {{$users->links()}}
@endsection
