@extends('layouts.app')

@section('title', 'Pigeons')

@section('header', 'All pigeons:')

@section('content')
    <ul>
        @foreach($pigeons as $pigeon)
            <li>
                <strong>{{$pigeon->name}}</strong> - {{$pigeon->ring_number}}
            </li>
        @endforeach
    </ul>

    {{$pigeons->links()}}
@endsection
